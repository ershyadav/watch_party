const express = require("express");
const app = express();

var bodyParser = require('body-parser');




app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false }));
const userRouter = require("./api/users/user.router");
const groups = require("./api/groups/group.router");

//app.use(express.json());

app.use("/api/users", userRouter);
app.use("/api/groups", groups);
app.post("/new" , function(req, res){
 console.log(req.body);
 res.send('hii');
});

app.get("/" , function(req, res){
  console.log('someone hitting url');
  res.send('Welcome in watch party');
 });
const port = 3000;
app.listen(port, () => {
  console.log("server up and running on PORT :", port);
});
